function idx = findClosestCentroids(X, centroids)
%FINDCLOSESTCENTROIDS computes the centroid memberships for every example
%   idx = FINDCLOSESTCENTROIDS (X, centroids) returns the closest centroids
%   in idx for a dataset X where each row is a single example. idx = m x 1 
%   vector of centroid assignments (i.e. each entry in range [1..K])
%

% Set K
K = size(centroids, 1);

% You need to return the following variables correctly.
idx = zeros(size(X,1), 1);

% ====================== YOUR CODE HERE ======================
% Instructions: Go over every example, find its closest centroid, and store
%               the index inside idx at the appropriate location.
%               Concretely, idx(i) should contain the index of the centroid
%               closest to example i. Hence, it should be a value in the 
%               range 1..K
%
% Note: You can use a for-loop over the examples to compute this.
%


%
%=========== for loop implementation ========
%

%{
m = size(X,1);

for i = 1:m    % for each training example find the nearest cluster centroid
    % initially set
    idx(i) = 1;
    prevDistance = 100000000; 
    for j = 1:K      % for each cluster
            distance = sum((X(i, :) - centroids(j,:)).^2);  % squared distance b/w X & jth centroid
            if distance < prevDistance
                idx(i) = j;
            end            
            prevDistance = distance;
    end
end
%}


%
%=========== vector implementation(litter faster than above) ========
%

%{
m = size(X,1);
for i = 1:m    % for each training example find the nearest cluster centroid
 
    [minDist, idx(i)] = min(sum((X(i,:) - centroids).^2, 2));
 
end
%}


%
%=========== vector implementation(faster than all) ===========
%

m = size(X,1);
distance = zeros(m, K);  % will contain the distance for all examples from every centroid in cols
                         % ex for m=1, the row will contain the distances of all the centorids from the first data point
                         
% for each centroid                         
for i = 1:K
    distance(:,i) = sum((X - centroids(i,:)).^2,2);
end

[minDist, idx] = min(distance');

% =============================================================

end


