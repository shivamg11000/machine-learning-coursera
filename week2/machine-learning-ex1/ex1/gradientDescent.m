function [theta, J_history] = gradientDescent(X, y, theta, alpha, num_iters)
%GRADIENTDESCENT Performs gradient descent to learn theta
%   theta = GRADIENTDESCENT(X, y, theta, alpha, num_iters) updates theta by 
%   taking num_iters gradient steps with learning rate alpha

% Initialize some useful values
m = length(y); % number of training examples
J_history = zeros(num_iters, 1);

for iter = 1:num_iters

    % ====================== YOUR CODE HERE ======================
    % Instructions: Perform a single gradient step on the parameter vector
    %               theta. 
    %
    % Hint: While debugging, it can be useful to print out the values
    %S       of the cost function (computeCost) and gradient here.
    %
    n = length(X(1,:));
    gradient = zeros(n,1);
    for j=1:n
        for i=1:m
            ithX = X(i, :);
            ithy = y(i);
            gradient(j,1) = gradient(j,1) + ((ithX*theta - ithy) * ithX(j)) / m; 
        end    
    end    
    theta = theta - (alpha)*gradient;




    % ============================================================

    % Save the cost J in every iteration    
    J_history(iter) = computeCost(X, y, theta);

end
   
end

